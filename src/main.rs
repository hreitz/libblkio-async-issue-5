fn main() -> Result<(), blkio::Error> {
    let mut iter = std::env::args().skip(1);

    let filename = iter.next().unwrap_or_else(|| "/tmp/test".into());

    let request_count = iter
        .next()
        .unwrap_or_else(|| "8388608".into())
        .parse::<usize>()
        .unwrap();

    let simultaneous = iter
        .next()
        .unwrap_or_else(|| "16".into())
        .parse::<usize>()
        .unwrap();

    let direct = match iter.next().unwrap_or_else(|| "cached".into()).as_str() {
        "direct" => true,
        "cached" => false,
        _ => {
            eprintln!("Invalid caching argument");
            std::process::exit(1);
        }
    };

    let mut blkio = blkio::Blkio::new("io_uring")?;

    blkio.set_str("path", &filename)?;
    blkio.set_bool("direct", direct)?;
    blkio.connect()?;
    assert!(!(blkio.get_bool("needs-mem-regions")?));
    blkio.set_i32("num-queues", 1)?;
    let flen = blkio.get_u64("capacity")?;
    blkio.start()?;

    let blkioq = blkio.get_queue(0)?;

    let mut base_buf = vec![42u8; simultaneous * 4096 + 4095];
    let mut head = 4096 - (base_buf.as_ptr() as usize) % 4096;
    if head == 4096 {
        head = 0;
    }

    let buf = &mut base_buf[head..(head + simultaneous * 4096)];

    let mut ofs = 0;
    let max_ofs = flen - 4096;

    let start = std::time::Instant::now();

    for i in 0..simultaneous {
        let buf_ofs = i * 4096;
        let slice = &mut buf[buf_ofs..(buf_ofs + 4096)];
        blkioq.read(
            ofs,
            slice.as_mut_ptr(),
            slice.len(),
            i,
            blkio::ReqFlags::empty(),
        );

        ofs += 4096;
        if ofs > max_ofs {
            ofs = 0;
        }
    }

    let mut completion_count = 0;
    let mut submitted_requests = simultaneous;

    while completion_count < request_count {
        // Safe because the element type is still MaybeUninit<_>
        let mut completions: [std::mem::MaybeUninit<blkio::Completion>; 64] =
            unsafe { std::mem::MaybeUninit::uninit().assume_init() };

        let n = blkioq.do_io(&mut completions, 1, None, None)?;
        completion_count += n;

        for completed in completions.iter_mut().take(n) {
            let completed = unsafe { completed.assume_init_mut() };
            let i = completed.user_data;

            if completed.ret < 0 {
                eprintln!("{} reports an error: {}", i, completed.ret);
            }
            if submitted_requests < request_count {
                let buf_ofs = i * 4096;
                let slice = &mut buf[buf_ofs..(buf_ofs + 4096)];
                blkioq.read(
                    ofs,
                    slice.as_mut_ptr(),
                    slice.len(),
                    i,
                    blkio::ReqFlags::empty(),
                );

                submitted_requests += 1;
                ofs += 4096;
                if ofs > max_ofs {
                    ofs = 0;
                }
            }
        }
    }

    let duration = std::time::Instant::now() - start;
    println!(
        "{} ops in {:?} = {} IOPS",
        completion_count,
        duration,
        completion_count as f64 / duration.as_secs_f64()
    );

    Ok(())
}
